import axios from "axios";

//Using axios to make api request

export default axios.create({
  baseURL: "https://itunes.apple.com/"
});
