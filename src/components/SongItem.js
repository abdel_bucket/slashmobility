import React, { Component } from "react";
import Favorite from "./Favorite";
import "../styles/scss/SongItem.scss";

export default class SongItem extends Component {
  //Every song item has: Album image - Song name - Album name
  render() {
    const {
      artworkUrl100,
      trackCensoredName,
      trackName,
      collectionName
    } = this.props.song;
    return (
      <div>
        <div className="image-cont">
          <img src={artworkUrl100} alt={trackCensoredName} />
        </div>
        <div className="titles">
          <h4>{trackName}</h4>
          <p>{collectionName}</p>
        </div>
        <Favorite favoritesCounter={this.props.favoritesCounter} />
      </div>
    );
  }
}
