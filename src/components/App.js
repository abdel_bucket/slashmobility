import React, { Component } from "react";
import Itunes from "../apis/Itunes";
import IonSearchBar from "./IonSearchBar";
import SongsShow from "./SongsShow";
import "../styles/scss/App.scss";

class App extends Component {
  state = { songs: [], counter: 0 };

  //Increment and Decrement the number of favorites when the user clicks on heart's icon
  favoritesCounter = liked => {
    liked
      ? this.setState({
          counter: this.state.counter - 1
        })
      : this.setState({
          counter: this.state.counter + 1
        });
  };

  //Make a request to Itunes API to get the response of list of songs
  onSearchArtist = async name => {
    const response = await Itunes.get("/search", {
      params: {
        term: name,
        entity: "song"
      }
    });

    console.log(response.data.results);
    this.setState({ songs: response.data.results, counter: 0 });
  };

  render() {
    return (
      <React.Fragment>
        <IonSearchBar
          onSearchArtist={this.onSearchArtist}
          counter={this.state.counter}
        />
        <SongsShow
          songs={this.state.songs}
          favoritesCounter={this.favoritesCounter}
        />
      </React.Fragment>
    );
  }
}

export default App;
