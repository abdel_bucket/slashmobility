import React, { Component } from "react";
import SongItem from "./SongItem";
import "../styles/scss/SongsShow.scss";

class SongsShow extends Component {
  render() {
    //After getting the response from itunes API, loop through the result
    const songs = this.props.songs.map(song => {
      return (
        <div key={song.trackId} className="song-item">
          <SongItem
            song={song}
            counter={this.props.counter}
            favoritesCounter={this.props.favoritesCounter}
          />
        </div>
      );
    });

    return <div className="songs-list">{songs}</div>;
  }
}

export default SongsShow;
