import React, { Component } from "react";
import "../styles/scss/IonSearchBar.scss";

class IonSearchBar extends Component {
  state = {
    temp: ""
  };

  //(Search input) Get user input value
  getInputValue = event => {
    this.setState({ temp: event.target.value });
  };

  //Get the submitted value and pass it to the parent component
  onSubmitValue = event => {
    event.preventDefault();
    this.props.onSearchArtist(this.state.temp);
  };

  render() {
    return (
      <header>
        <div className="nav">
          <input type="checkbox" id="nav-check" />
          <div className="nav-btn">
            <label htmlFor="nav-check">
              <span />
              <span />
              <span />
            </label>
          </div>
          <div className="nav-header">
            <svg className="hand-icon" viewBox="0 0 24 24">
              <path
                fill="#ffffff"
                d="M10,9A1,1 0 0,1 11,8A1,1 0 0,1 12,9V13.47L13.21,13.6L18.15,15.79C18.68,16.03 19,16.56 19,17.14V21.5C18.97,22.32 18.32,22.97 17.5,23H11C10.62,23 10.26,22.85 10,22.57L5.1,18.37L5.84,17.6C6.03,17.39 6.3,17.28 6.58,17.28H6.8L10,19V9M11,5A4,4 0 0,1 15,9C15,10.5 14.2,11.77 13,12.46V11.24C13.61,10.69 14,9.89 14,9A3,3 0 0,0 11,6A3,3 0 0,0 8,9C8,9.89 8.39,10.69 9,11.24V12.46C7.8,11.77 7,10.5 7,9A4,4 0 0,1 11,5M11,3A6,6 0 0,1 17,9C17,10.7 16.29,12.23 15.16,13.33L14.16,12.88C15.28,11.96 16,10.56 16,9A5,5 0 0,0 11,4A5,5 0 0,0 6,9C6,11.05 7.23,12.81 9,13.58V14.66C6.67,13.83 5,11.61 5,9A6,6 0 0,1 11,3Z"
              />
            </svg>
            <form onSubmit={this.onSubmitValue}>
              <input
                type="text"
                placeholder="Search"
                value={this.state.temp}
                onChange={this.getInputValue}
              />
            </form>
            <svg className="search-icon" viewBox="0 0 24 24">
              <path
                fill="#ffffff"
                d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z"
              />
            </svg>
          </div>
          <div className="favorites">
            <span className="counter">{this.props.counter}</span>
            <svg className="heart-icon" viewBox="0 0 24 24">
              <path
                fill="#ffffff"
                d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z"
              />
            </svg>
          </div>
          <div className="desc">
            <div>
              <h3>
                <span>PRUEBA TÉCNICA - FRONTEND JUNIOR</span> <br />
                SLASHMOBILITY 2019 &#9400; ABDELBARI AJHIR <br />
              </h3>
              <br />
              <img src="logo.png" alt="slashmoblity" />
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default IonSearchBar;
